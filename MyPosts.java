import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class MyPosts extends AbstractTest {

    //Страница 3 моих постов, пользователь авторизован
    @Test
    void page3(){
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("sort", "createdAt")
                .queryParam("page", 3)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[]", notNullValue())
        ;
    }

    //Сортировка по убыванию, пользователь авторизован
    @Test
    void orderDesc(){
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .statusCode(200);
    }

    //Сортировка 4 страницы по убыванию, пользователь авторизован
    @Test
    void orderDescPage4(){
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", 4)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .statusCode(200);
    }

    // Отсутствие постов на несуществующей странице, пользователь авторизован
    @Test
    void notPostToPage(){
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("sort", "createdAt")
                .queryParam("page", 120)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", nullValue())
        ;
    }

    //Запрос страницы неавторизованным пользователем
    @Test
    void notAuthUserMyPosts(){
        given()
                .queryParam("sort", "createdAt")
                .queryParam("page", 2)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .assertThat()
                .statusCode(401)
                .body("message", equalTo("Auth header required X-Auth-Token"));
        ;
    }






}

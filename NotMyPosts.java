import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

public class NotMyPosts extends AbstractTest{

    @Test
    void page1OrderASC(){
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("order", "ASC")
                .queryParam("page", 1)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .statusCode(200);


    }

    @Test
    void page10OrderDESC() {
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("order", "DESC")
                .queryParam("page", 10)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .header("X-Powered-By", "PHP/8.1.10")
                .contentType("application/json");
    }

    @Test
    void emptyPage(){
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("page", 10000)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", nullValue())
        ;

    }

    //Сортировка по дате публикации
    @Test
    void sortCreatedAt(){
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .contentType("application/json");
    }

    //Страница 0
    @Test
    void page0(){
        given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("page", 0)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .contentType("application/json");
    }

    //Не авторизованныей пользователь
    @Test
    void noAuthUser(){
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("page", 23)
                .when()
                .get(getBase_url()+"api/posts")
                .then()
                .assertThat()
                .statusCode(401)
                .contentType("application/json")
                .body("message", equalTo("Auth header required X-Auth-Token"));
    }

}
